console.log("Hello world! Das ist/Je suis Oleksander... :D");

async function loginClick() { <!-- async mowi ze funkcja jest asynchroniczna-->
    var inputEmail;
    inputEmail = document.getElementById("email").value;
    window.alert(inputEmail);
    var password;
    password = document.getElementById("password").value;
    var url;
    url = `http://localhost:8080/api/login?login=${inputEmail}&password=${password}`;
    console.log(url);
    // fetch(url,{method:"post"}).then(n=>console.log(n.text()));
    var response = await fetch(url, {method: "post"});
    var message = await response.text();
    console.log(message);
}

async function loginSubmit(form, event) { <!-- async mowi ze funkcja jest asynchroniczna-->
    event.preventDefault();
    var data;
    data = new URLSearchParams(new FormData(form));
    var url;
    url = `http://localhost:8080/api/login`;
    console.log(url);
    // fetch(url,{method:"post"}).then(n=>console.log(n.text()));
    var response = await fetch(url, {method: "post", body: data});
    var message = await response.text();
    if (response.status === 200){
        window.location.href = "index.html";
    }else {
        var windowAlert;
        windowAlert=document.getElementById("alert");
        windowAlert.hidden=false;
        windowAlert.innerText=message;
    }
    console.log(message);
    return false;
}